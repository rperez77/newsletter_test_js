var express = require('express');
var logger = require('morgan');
var path = require('path');
var lessMiddleware = require('less-middleware');
var app = express();

app.use(logger('dev'));
app.use(lessMiddleware('/less', {
  dest: '/styles',
  pathRoot: path.join(__dirname, 'public')
}));
app.use(express.static(path.join(__dirname, 'public')));

module.exports = app;
