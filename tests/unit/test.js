/**
 * Dom functions tests
 * Roger Alberto Perez Guerra
 * roger.a.guerra@gmail.com
 */

describe('DOM Tests', function () {
  /**
   * Step1: Test for create element function
   */
  describe('Create element function', function(){

    var width = '450px';
    var element = RendererModule.createTableElement('table',width);
    document.body.appendChild(element);
    var el = document.querySelector('table');

    it('should be in the doom', function () {
      expect(el).to.not.equal(null);
    });

    it('should be the right element', function () {
      expect(el.nodeName).to.equal('TABLE');
    });

    it('should have the right width', function () {
      expect(el.style.width).to.equal(width);
    });
    
  })

  describe('Create image function', function(){

    var src = 'https://i.ytimg.com/vi/m5d1FlSeF-M/maxresdefault.jpg';
    var element = RendererModule.createImage(src,'alt','title');
    document.body.appendChild(element);
    var el = document.querySelector('img');

    it('should be in the doom', function () {
      expect(el).to.not.equal(null);
    });

    it('should be the right element', function () {
      expect(el.nodeName).to.equal('IMG');
    });

    it('should have the right src', function () {
      expect(el.src).to.equal(src);
    });

    
  })

  describe('Create span function', function(){

    var align = 'left';
    var content = 'Hi!!'
    var element = RendererModule.createSpan(align,content);
    document.body.appendChild(element);
    var el = document.querySelector('span');

    it('should be in the doom', function () {
      expect(el).to.not.equal(null);
    });

    it('should be the right element', function () {
      expect(el.nodeName).to.equal('SPAN');
    });

    it('should have content', function () {
      expect(el.innerHTML).to.equal(content);
    });


    it('should have the right align', function () {
      expect(el.style.textAlign).to.equal(align);

    });
    
  })

});

describe('Function test', function(){
  describe('Load structture', function(){
    it('should load file', function() { // added 'done' as parameter
      uploadFile.onChange('../../data/template.json', function(e) {
        expect(e.target.result).to.not.equal(null);
        expect(e.target.result).to.be.an('string');
      });
    });

    
  })
})
