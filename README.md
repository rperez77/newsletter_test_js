# Newsletter Renderer

## Roger Perez Solution

#### Requirements

To execute and test the exercise you will need nodejs, and  npm.

#### Walkthrough

To execute the exercise:

* Move to the main directory of the file:
```
cd newsletter_test_js
```
* Run the command:
```
npm install && npm start
```
* In Google Chrome navigate to [http://localhost:3000](http://localhost:3000)

To execute the tests:

* Into to the main directory of the project, run these commands:
```
npm test
```

* There are two json examples in the "data" folder

### Scripts

* ```rendererModule.js```: this module contains the functions to create the email structure.
* ```uploadFile.js```: this module contains the function to read a json file and return content as a string.
* ```main.js```: this js calls the file function and invokes the functions to render the email template.

### Styles

The styles are generated using a CSS Pre-Compilers (Less):

* ```reset.less```: reset default browser's styles.
* ```main.less```: styles needed to render the page






