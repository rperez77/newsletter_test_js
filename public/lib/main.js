/**
 * Main
 * Roger Alberto Perez Guerra
 * roger.a.guerra@gmail.com
 */

(function() {

	/**
	 * Call the function to get the email structure
	 */

	document.getElementById('json-file').addEventListener('change', function() {
		if ('files' in this) {
			uploadFile.onChange(this.files[0],function(e){
				try {
        	var data = JSON.parse(e.target.result);
        	RendererModule.createMailStructure(data);
		    } catch (e) {
		    	alert(e);
		    }		
			});
		}
	});

})();
