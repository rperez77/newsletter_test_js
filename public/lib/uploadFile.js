/**
 * Upload File
 * Roger Alberto Perez Guerra
 * roger.a.guerra@gmail.com
 */

var uploadFile = (function(){

  /**
   * Function to read a json file
   */
  var onChange = function (file, onLoadCallback) {
    var reader = new FileReader();
    reader.onload = onLoadCallback;
    reader.readAsText(file);
  };

  return {
    onChange: onChange
  };

}());
