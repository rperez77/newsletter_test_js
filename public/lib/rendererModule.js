/**
 * Rendet module
 * Roger Alberto Perez Guerra
 * roger.a.guerra@gmail.com
 */

 var RendererModule = (function (mod) {
  
  /**
   * Function to create the structure of the email
   */
  mod.createMailStructure = function (data) {
    var section = document.getElementById("newsletter-rendered");
    section.innerHTML = "";
    var mailStructure = this.createTableElement('table','600px');
    mailStructure.style.margin = "0 auto";
    mailStructure.style.background = "white";
    for ( var key in data.rows ) {
      if ( data.rows.hasOwnProperty(key) ) {
        var parentTable = this.createTableElement('table','100%');
        var tbody = this.createTableElement('tbody');
        // Validate if element has childs
        if ( data.rows[key].columns.length > 0 ) {
          parentTable.appendChild(tbody);
          var tr = this.createTableElement('tr');
          var widthTd = 100/data.rows[key].columns.length;
          widthTd = widthTd + 'px';
          //Iteration for create childs
          for ( var subKey in data.rows[key].columns ) {
            var td = this.createTableElement('td',widthTd);
            if ( data.rows[key].columns[subKey].type=="text" ){
              var content = this.createSpan('center',data.rows[key].columns[subKey].content);
            } else {
              var content = this.createImage(data.rows[key].columns[subKey].content,'image','image');
            }
            td.appendChild(content);
            tr.appendChild(td);
          }
          parentTable.appendChild(tr);
        }
        mailStructure.appendChild(parentTable);
      }
    }
    section.appendChild(mailStructure);
  }

  /**
   * Function to create a image element with alt and title
   */
  mod.createImage = function (src, alt, title) {
    var img= document.createElement('img');
    img.src= src;
    if ( alt!=null ) img.alt= alt;
    if ( title!=null ) img.title= title;
    img.style.width = '100%';
    img.style.height = 'auto';
    return img;
   }

  /**
   * Function to create a span element with content
   */
  mod.createSpan = function (align, content) {
    var span= document.createElement('span');
    align = typeof align !== 'undefined' ? align : 'center';
    span.style.textAlign = align;
    if ( typeof content !== 'undefined' ) {
      span.innerHTML = content;
    }
    span.style.display = 'block';
    span.style.width = '100%';
    return span;
   }

  /**
   * Function to create html elements dynamically
   */
  mod.createTableElement = function (type,width) {
    var element = document.createElement(type);
    if ( typeof width !== 'undefined' ) {
      element.style.width = width;
    }
    return element;
  }

  return mod;

})(RendererModule || {});

